using System;
using System.Collections.Generic;
using Ink.Runtime;
using TMPro;
using Unity.Burst;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Padmin
{
    public class DialogueSystem : MonoBehaviour
    {
        private static DialogueSystem instance;
        private PlayerControls controls;

        public GameObject mainPanel;
        public TextMeshProUGUI speakerName;
        public TextMeshProUGUI dialogueText;
        public GameObject choicesPanel;
        public Transform choicesParent;
        public GameObject choiceObject;
        public bool isDialoguePlaying;
        
        public static event Action OnDialogueStart;
        public static event Action OnDialogueExit;

        private Story currentStory;
        private List<GameObject> activeChoices;
        private Interact currentInteract;
        private bool isChoosingDialogue;

        private void Awake()
        {
            if (instance) return;
            instance = this;

            controls = new PlayerControls();
            activeChoices = new List<GameObject>();
        }
        
        public static DialogueSystem Get()
        {
            return instance;
        }

        public void StartDialogue(TextAsset inkJson, string interactName, Interact interact)
        {
            currentStory = new Story(inkJson.text);
            isDialoguePlaying = true;
            mainPanel.SetActive(true);
            speakerName.text = interactName;
            currentInteract = interact;
            
            OnDialogueStart?.Invoke();
            
            ContinueStory();
        }

        private void ContinueStory()
        {
            if (isChoosingDialogue) return;
            if (currentStory.canContinue)
            {
                dialogueText.text = currentStory.Continue();
            }
            else
            {
                if (currentStory.currentChoices.Count == 0)
                {
                    ExitDialogue();
                }
                else
                {
                    foreach (var item in currentStory.currentChoices)
                    {
                        var choice = Instantiate(choiceObject, choicesParent);
                        choice.GetComponentInChildren<TextMeshProUGUI>().text = item.text;
                        activeChoices.Add(choice);
                    }
                    isChoosingDialogue = true;
                    choicesPanel.SetActive(true);
                }
                 
            }
        }

        public void SubmitChoice(int index)
        {
            currentStory.ChooseChoiceIndex(index);
            foreach (var item in activeChoices)
            {
                Destroy(item);
            }
            choicesPanel.SetActive(false);
            isChoosingDialogue = false;
            ContinueStory();
        }
        
        private void OnDownButton(InputAction.CallbackContext obj)
        {
            
        }

        private void OnUpButton(InputAction.CallbackContext obj)
        {
            
        }

        private void OnSubmitButton(InputAction.CallbackContext obj)
        {
            if (!isDialoguePlaying) return;
            ContinueStory();
        }

        private void ExitDialogue()
        {
            isDialoguePlaying = false;
            mainPanel.SetActive(false);
            dialogueText.text = "";
            currentInteract.OnFinishDialogue();
            
            OnDialogueExit?.Invoke();
        }

        private void OnEnable()
        {
            controls.UI.Submit.Enable();
            controls.UI.Up.Enable();
            controls.UI.Down.Enable();

            controls.UI.Submit.performed += OnSubmitButton;
            controls.UI.Up.performed += OnUpButton;
            controls.UI.Down.performed += OnDownButton;
        }

        private void OnDisable()
        {
            controls.UI.Submit.Disable();
            controls.UI.Up.Disable();
            controls.UI.Down.Disable();
        }
        
        private void OnDestroy()
        {
            instance = null;
        }
        
    }
}