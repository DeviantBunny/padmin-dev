using System;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Padmin
{

    public class Doors : MonoBehaviour
    {
        public Transform doorTrans;
        public Vector3 initialRotation;
        public Vector3 finalRotation;
        public float speed = 0.5f;
        public BoxCollider2D col;
        

        [Button("Open")]
        public void Open()
        {
            doorTrans.DORotate(finalRotation, speed, RotateMode.Fast).SetEase(Ease.InOutSine);
        }

        [Button("Close")]
        public void Close()
        {
            doorTrans.DORotate(initialRotation, speed, RotateMode.Fast).SetEase(Ease.InOutSine);
        }
    }
}