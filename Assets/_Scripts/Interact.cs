using System;
using UnityEngine;
using UnityEngine.Events;

namespace Padmin
{
    
    public class Interact : MonoBehaviour
    {
        public string interactName;
        public GameObject visualCue;
        public TextAsset inkJson;
        public UnityEvent onFinishDialogue;

        private bool playerInRange;

        private void Awake()
        {
            visualCue.SetActive(false);
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            visualCue.SetActive(true);
            playerInRange = true;
            
            if (TryGetPlayer(other, out var player))
            {   
                SetInteractRef(player, true);
            }
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            visualCue.SetActive(false);
            playerInRange = false;

            if (!TryGetPlayer(other, out var player)) return;
            if (player.Interact == this)
            {
                player.Interact = null;
            }
        }

        public void OnInteract()
        {
            if (!playerInRange) return;
            DialogueSystem.Get().StartDialogue(inkJson, interactName, this);
        }

        public void OnFinishDialogue()
        {
            onFinishDialogue?.Invoke();
        }

        private void SetInteractRef(PlayerController player, bool value)
        {
            var reference = value ? this : null;
            player.Interact = reference;
        }

        private bool TryGetPlayer(Component col, out PlayerController player)
        {
            var playerRef = col.GetComponent<PlayerController>();
            if (playerRef == null)
            {
                player = null;
                return false;
            }
            else
            {
                player = playerRef;
                return true;
            }

        }
    }
}