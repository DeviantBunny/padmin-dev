using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEditor.Rendering;
using UnityEngine;
using UnityEngine.Rendering.Universal;

namespace Padmin
{
    public class LightFlicker : MonoBehaviour
    {
        public float minIntensity;
        public float maxIntensity;
        public float speed = 0.2f;
        
        private Light2D lightBulb;
        private float lightIntensity;
        private Tween flicker;

        private void Awake()
        {
            lightIntensity = minIntensity;
            lightBulb = GetComponent<Light2D>();
            flicker = DOTween.To(() => lightIntensity, x => lightIntensity = x, maxIntensity, speed)
                .SetLoops(-1, LoopType.Yoyo);
        }

        private void Update()
        {
            lightBulb.intensity = lightIntensity;
        }

        private void OnDisable()
        {
            flicker.Kill();
        }
    }
}