using System;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Padmin
{
    public class PlayerController : MonoBehaviour
    {
        public float moveSpeed;
        
        private PlayerControls controls;
        private InputAction movement;
        private Rigidbody2D body;
        private Vector2 moveDirection;

        public Interact Interact { get; set; }

        private void Awake()
        {
            controls = new PlayerControls();
            body = GetComponent<Rigidbody2D>();
            DialogueSystem.OnDialogueStart += OnDialogueStart;
            DialogueSystem.OnDialogueExit += OnDialogueExit;
        }

        private void OnDialogueExit()
        {
            controls.Enable();
        }

        private void OnDialogueStart()
        {
            controls.Disable();
        }

        private void OnInteract(InputAction.CallbackContext obj)
        {
            if (Interact == null) return;
            Interact.OnInteract();
            
        }

        private void Update()
        {
            moveDirection = movement.ReadValue<Vector2>();
        }

        private void FixedUpdate()
        {
            body.velocity = new Vector2(moveDirection.x * moveSpeed, moveDirection.y * moveSpeed);
        }

        private void OnEnable()
        {
            movement = controls.Player.Movement;
            movement.Enable();
            
            controls.Player.Interact.Enable();
            controls.Player.Interact.performed += OnInteract;
        }

        private void OnDisable()
        {
            controls.Player.Movement.Disable();
            controls.Player.Interact.Disable();
        }
    }
}