using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.Universal;

namespace Padmin
{
    public class LightSwitch : MonoBehaviour
    {
        private Light2D lightBulb;

        private void Awake()
        {
            lightBulb = GetComponent<Light2D>();
        }

        public void TurnOn()
        {
            lightBulb.enabled = true;
        }

        public void TurnOff()
        {
            lightBulb.enabled = false;
        }
    }
}