using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Padmin;
using UnityEngine;

public class LevelDynamics : MonoBehaviour
{
    public GameObject firstSasa;
    public GameObject secondSasa;
    public bool hasFoundPen;
    public bool hasFoundTablet;
    public TextAsset finalDialogue;

    public void FindPen()
    {
        hasFoundPen = true;
        CheckForItems();
    }

    public void FindTablet()
    {
        hasFoundTablet = true;
        CheckForItems();
    }

    private void CheckForItems()
    {
        if (hasFoundPen && hasFoundTablet)
        {
            SwitchDialogue();
        }
    }

    private void SwitchDialogue()
    {
        secondSasa.GetComponentInChildren<Interact>().inkJson = finalDialogue;
    }

    public void Disappear()
    {
        firstSasa.GetComponentInChildren<Interact>().enabled = false;
        firstSasa.transform.DOScale(0f, 0.5f);

        secondSasa.transform.DOScale(0.65f, 0.5f);
    }
}
