using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Padmin
{
    
    public class GenericTrigger : MonoBehaviour
    {
        public UnityEvent onEnter;
        public UnityEvent onExit;
        private void OnTriggerEnter2D(Collider2D other)
        {
            onEnter.Invoke();
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            onExit.Invoke();
        }
    }
}