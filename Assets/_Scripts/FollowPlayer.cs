using UnityEngine;

namespace Padmin
{
    public class FollowPlayer : MonoBehaviour
    {
        public Transform player;
        public float lerpSpeed = 0.2f;

        private Vector3 offset;

        private void Start()
        {
            offset = transform.position - player.position;
        }

        private void FixedUpdate()
        {
            /*var newPosition = (Vector2) player.position + offset;
            transform.position = newPosition;*/
            
            var targetPos = player.position + offset;
            transform.position = Vector3.Lerp(transform.position, targetPos, lerpSpeed * Time.deltaTime);
        }
    }
}