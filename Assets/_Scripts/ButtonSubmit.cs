using System;
using UnityEngine;
using UnityEngine.UI;

namespace Padmin
{

    public class ButtonSubmit : MonoBehaviour
    {
        private Button button;
        private int buttonIndex;

        private void Start()
        {
            button = GetComponent<Button>();
            buttonIndex = transform.GetSiblingIndex();
            
            button.onClick.AddListener(SelectChoice);
        }

        private void SelectChoice()
        {
            DialogueSystem.Get().SubmitChoice(buttonIndex);
        }

        private void OnDestroy()
        {
            button.onClick.RemoveAllListeners();
        }
    }
}