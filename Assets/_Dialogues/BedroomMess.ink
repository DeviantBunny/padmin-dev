It's a hot mess of clothes, blankets, belts and towels.
An evergrowing mound of textile that threatens to eat the unwary.
If the thing is in this linen nightmare, good luck finding it!
    * [Is it?]
- No. 
But it would be difficult to find it if it were here. 
    * [So why bring it up?]
- I'm just saying... You know. This guy is messy!
    * [What else is new?]
- I mean come on. How difficult is it to fold the clothes and put 'em back into the closet? 
    * [Mhm]
- Half of this stuff is winter clothes and it's like 20 degrees outside. 
    * [I know]
- Thanks. You're a good listener. 
    * [Sure. Hey, do you know where the thing is?]
- The thing? Oh, it's in the living room. One of them anyway.
Don't get me started on the living room. 
All tidy and neat.
    * [I wouldn't exactly call it tidy...]
- Well, there's no clothes lying around all the time, is there? 
    * [I guess]
- Tidy! Anyway, go find your thing. 