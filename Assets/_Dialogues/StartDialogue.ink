This is a Saša. It's a fairly common human of a geek subtype. It sustains itself with hugs, energy drinks and an occasional nap. 
Its mouth moves and sounds come out. You think its trying to communicate. 
"Hey you", it says.
    * [Hey you]
        It makes a thing with its face. Its lips stretch out a bit. Is it... smiling?
    * [Give it a hug]
        This feels nice. You think it likes it too.
    * [Give it an energy drink]
        You don't have an energy drink. And he's probably had two already. At the same time.
    - "You look cute".
        * [You're biased]
            "Doesn't change the fact that you're cute."
        * [You look cuter]
            "Only from certain angles."
        * [Your butt looks cute]
            "The butt appreciates it."
    - "How was your day?"
        * [Fine]
        * [It was fine]
        * [Eh, fine]
    - "Fine?"
        * [Fine]
        * [Fine]
        * [Fine]
    - "Fine. So... I have something for you."
        * [I know! You made a game for me! That's so cute!]
            "I know, right? But that's not all!"
        * [You... got me a game? That's lame.]
            "Ouch. Ok, but that's not it."
    - "The real thing is hidden somewhere around. Well, two things in two somewheres, actually."
        * [Yay! A treasure hunt! (...is totally something I would say)]
            "Yay! I'm so glad I got your tone of voice right."
        * [You don't give two presents outside of funerals. You never listen to me!]
            "Yeah, but two somethings PLUS the game are three somethings."
                ** [Smartass]
                ** [Fine]
            -- "There you go."
        * [You made me play your dumb game AND you want me to work for my present?]
            "Heeey! Rude! The Min in my head would never pick this choice!"
                ** [Then why did you include it?]
            -- "Well, there's always an "evil" dialogue option in these kinds of games."
    - "Anyway, there are two somethings hidden in two different places. Together, they make a greater whole."
        * [Is that like a methaphor or something?]
    - "What? No. It's like two pieces of a thing."
        * [Cool. So how do I find them?]
        * [It's totally a methaphor.]
            "Fine, it's a methaphor."
            ** [I knew it! So how do I find them?]
    - "Well, you can rummage through all my stuff until you find them both, or..."
    "You can play the game!" 
    "You can "talk" to stuff in the apartment. Sometimes they even talk back! But importantly, it will help you locate the things in realspace."
        * [But what if I get bored?]
    - "You can always ask me nicely and I'll give it to you."
        * [Fine]
            "Fine?
                ** [Fine]
                    "Fine."
        * [Okay. But uplift me first]
            WHOOOP. CRUNCH. "There you go."
                ** [Thank you]
        * [You're gonna... give it to me?]
            "Naughty!"
    - "Good luck! I'll just stand there by the couch while you're searching. I would sit on the couch, but I was too lazy to animate that."
        * [You are cute for making the game.]
        * [You are lame for making the game.]
        * [You are cute, but not for making the game. The game sucks.]
    - "Thanks? Go do!" 
        * [Bye]
            He waves and shrinks himself out of existence.
    