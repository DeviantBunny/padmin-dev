For somebody who hardly ever reads, this guy has a lot of books. 
There's also a few interesting looking board games lying around. 
Not that he ever plays them. 
    * [Hello?]
    * [I'm looking for the thing]
- There's quite a few comic books here too. 
And guess what? He never reads them! 
    * [I'm right here, you know]
- And what's up with all the Legos?
How old is he? Like, twelve? 
    * [Excuse me, is the thing here?]
- Oh, right. The thing, you say? 
There's quite a few things here. 
There's a hat, for example. Do you want a hat? 
    * [No, I don't want the hat. I was told there's a thing here.]
- Well, we have a lot of things. Are you sure it's not the hat?
Well, maybe it's this clock! 
    * [I don't think it's the clock]
- Oh, right. You're probably looking for the box.
    * [There's like twenty boxes here]
- No, no. The new box. The thing! 
I'm pretty sure that's it. 
Here it is, with the other boxes, just down there.
Whiteish, with a little fruit on the cover. 
    * [Oh, the thing!]
- The very same. Congrats! You found the thing. 
Go get it! 
