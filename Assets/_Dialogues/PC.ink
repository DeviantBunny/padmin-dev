This is Saša's desk. It's fairly messy.
Stacks of notes, notebooks and notes about books cover most of its surface.
A cup of day-old tea sits in the corner. It smells like smoked wood. You're not sure why anyone would drink that. 
The screen shows a bunch of letters and numbers, but they don't make much sense. The colors are pretty, though.
    * [Is the thing here?]
- Probably not. It would be kinda underwhelming if the thing was hidden right under your nose, right?
    * [But I just got here!]
- But the REAL you was sitting here already. It's a bit confusing, really. 
It makes you question the very nature of our reality, isn't it?
    * [Umm, no. Not really]
    * [Does it?]
- What if we're all living in a simulation? 
The "real" you is just another character in a game that the REAL real you...'r boyfriend made?
    * [Ummm]
- And even SHE is just another character in another game that somebody made. 
And so on and on. It makes my head spin. 
    * [Nerd]
    * [I'm just gonna go now]
Yeah, anyway. Good luck finding it! If it exists at all. 
